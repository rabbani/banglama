<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
          integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Coda|Tangerine" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text" rel="stylesheet">
    <script type="text/javascript" src="js/modernizr.custom.86080.js"></script>
    <title> BanglaMa Design</title>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg" style="z-index: 2000">
    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    About
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="whoweare.php">Who we are</a>
                    <a class="dropdown-item" href="missionvision.php">Mission & Vision</a>
                    <a class="dropdown-item" href="profile.php">Company Profile</a>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Message
                    </a>
                    <div class="dropdown-menu message" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="chairman.php">Chairman Message</a>
                        <a class="dropdown-item" href="md.php">Managing Director message</a>

                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="services.php">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="project.php">Project</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="gallery.php"> Gallery </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="career.php">Career</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact.php"> Contact Us</a>
            </li>

        </ul>
    </div>
</nav>

<div class="career-body">
    <div class="body-shade">
        <div class="container">

            <div class="mt-5 career">

                <h3 class="text-center title-text">Career Opportunity</h3>
                <p class="text-justify services-text">BanglaMa Design Ltd. are a high performing team having dedication and passion for uniqueness of achievement, initiative, and professionalism with intensity and energy that sets us apart. We make a positive impact in our communities through continuous innovation and change, working with talent at every level. In our relentless pursuit we are on the lookout for motivated, talented additions to our continuously expanding organization, if you have right skills we invite applications for the following posts.</p>


            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="mt-5">
        <h3>Accounts Officer</h3>
        <p>Vacancy : 02</p>
        <p class="float-right" style="font-size: 14px;">Application Deadline : June 15, 2018</p>
        <br>
        <p>Job Context</p>
        <p>Job Location: Head Office </p>
        <div class="accounts-job">
            <h4>Job Responsibilities</h4>
            <p>Manage day to day accounting records including Tally entries
                Cash & banking transaction management
                Prepare periodical financial reports and analyses
                Cash flow reports and budget/forecast related works
                Manage company TAX/VAT and other regulatory issues.</p>
            <div><b>Employment Status</b></div>
            <br>
            <p>Full-time</p>
            <p><b>Educational Requirements</b></p>
            <p>MBA in finance or Masters in Accounting </p>
            <p><b>Experience Requirements</b></p>
            <p>At least 3 year(s)</p>
        </div>
        <div class="row">
            <button type="button" class="btn btn-danger btn-sm ml-auto" id="accounts_more">More</button>
        </div>
    </div>
</div>

<hr>

<div class="container">
    <div class="mt-5">
        <h3>Marketing Officer</h3>
        <p>Vacancy : 02</p>
        <p class="float-right" style="font-size: 14px;">Application Deadline : July 31, 2018</p>
        <br>
        <p>Job Context</p>
        <p>Job Location: Head Office </p>
        <div class="marketing-job">
            <h4>Job Responsibilities</h4>
            <p>Carry out direct IT/Web/Software marketing activities according to the goal by own and set up
                by
                the Team Leader.
                Good knowledge about IT Training/Web Development/ Software Application.
                Search Productive customer & communicate with them professionally.
                Good knowledge about web application and software functionality
                Enable to Talk Efficiently to client over phone and visit client office.
                Maintain quality relationships with current and new clients.
                Works with sales team to develop sales goals and action plans.
                Staying up-to-date and knowledgeable on our and global software.
                Computer skills with Microsoft Excel, Software Operation, Project Documentation, Word,
                Internet
                Browse & Search, Power Point Presentation, Email.
                Self-motivated and goal-oriented.
                To explore and visit regularly to Clients.
                Report daily/weekly sales report to reporting boss.
                Achieve monthly sales target.
                Analyzed market conditions and found new customer for business developments.
                Giving solution on problems and queries raised by customers.
                Applicant must be dynamic and good wishes or willing to work under pressure.
            </p>
            <div><b>Employment Status</b></div>
            <br>
            <p>Full-time</p>
            <p><b>Educational Requirements</b></p>
            <p>MBA in finance or Masters in Accounting </p>
            <p><b>Experience Requirements</b></p>
            <p>At least 3 year(s)</p>
        </div>
        <div class="row">
            <button type="button" class="btn btn-danger btn-sm ml-auto" id="marketing_more">More</button>
        </div>
    </div>
</div>
<hr>

<div class="container">
    <div class="mt-5">
        <h3>Marketing Executive</h3>
        <p>Vacancy : 03</p>
        <p class="float-right" style="font-size: 14px;">Application Deadline : July 31, 2018</p>
        <br>
        <p>Job Context</p>
        <p>A well reputed company is looking for a young, energetic self-motivated Marketing Executive.</p>
        <p>Job Location: Head Office </p>
        <h4>Job Responsibilities</h4>
        <div class="marketing-executive-job">
            <ul>
                <li>Market visit, client hunt, creating market opportunity.</li>
                <li>To discuss technically if any clients wants to have technical discussion.</li>
                <li>Conducting market research such as customer questionnaires and focus groups.</li>
                <li>Create new customer, increase of market share, development of marketing strategic plan,
                    and
                    works
                    for achieving the plan.
                </li>
                <li>Tracking competitor's moves, gathering market feedback and helping to determine
                    appropriate
                    positioning and feature to gain competitive advantage in a targeted segment.
                </li>
                <li>To work as an effective member of the Marketing and Sales team to achieve the long term
                    goals of
                    the department.
                </li>
                <li> To keep update the monthly sales progress to the management.</li>
            </ul>

            <div><b>Employment Status</b></div>
            <br>
            <p>Full-time</p>
            <p><b>Educational Requirements</b></p>
            <p>*Minimum Graduation in any discipline or BBA/ MBA (Major in Marketing)
                *Educational qualification will be relaxed with the deserving candidates. </p>
            <p><b>Experience Requirements</b></p>
            <p>At least 3 year(s)</p>
            <h4 class="text-center">Apply Procedure</h4>
            <p>
                Interested candidates are requested to send their application with detailed CV, attested
                copies
                of
                all academic & experience certificates and 2 copies of passport size photographs to HR
                Department at
                60 Purana paltan Line, Dhaka-1000.

                Send your CV to  info@banglamadesign.com
                Application Deadline: July 31, 2018</p>
        </div>
        <div class="row">
            <button type="button" class="btn btn-danger btn-sm ml-auto" id="marketing_executive_more">More
            </button>
        </div>

    </div>
</div>
<hr>

<div class="container-fluid">
    <div class="our-clients">
        <div class="our-clients-title text-center">Our Clients</div>
        <div class="our-client-list text-center row">
            <div class="owl-carousel">
                <div class="col">
                    <img src="images/c1.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c2.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c3.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c4.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c5.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c6.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c7.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c8.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c9.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c10.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c11.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="footer row p-4">
        <div class="col text-center br-1">
            <div class="footer-logo">
                <img src="images/logo.png" alt="logo">
            </div>
            <br>
            <a href="http://banglamadesign.com:2095/" target="_blank" class="btn btn-outline-danger btn-sm btn-webmail">WebMail</a>
        </div>
        <div class="col text-center">
            <h3>
                Follow Us On
            </h3>
            <div class="social">
                <a href="https://www.facebook.com/banglamadesignltd/" target="_blank"><i class="fab fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UCQmFWII5iS1rF9VfNAkgapw" target="_blank"><i class="fab fa-youtube"></i></a>
                <i class="fab fa-linkedin"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-google-plus-square"></i>
                <i class="fab fa-instagram"></i>
            </div>
        </div>
        <div class="col">
            <h6 class="text-danger">
                Contact Info
            </h6>
            <p class="footer-address">
                <i class="fas fa-home text-danger"></i> 60 Purana Palton Line, Dhaka-1217
                <br><i class="fas fa-phone text-danger"></i> +88028312417, 9352282
                <br><i class="fas fa-mobile-alt text-danger"></i> &nbsp;+88 01936208414
                <br><i class="fas fa-envelope-open text-danger"></i> info@banglamadesign.com | banglamadesignltd@gmail.com
            </p>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
        integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
        crossorigin="anonymous"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
<script>
    $(document).ready(function () {
        $('.accounts-job').hide();
        $('.marketing-job').hide();
        $('.marketing-executive-job').hide();
        $('#accounts_more').on('click', function () {
            $('.accounts-job').toggle();
        });

        $('#marketing_more').on('click', function () {
            $('.marketing-job').toggle();
        });
        $('#marketing_executive_more').on('click', function () {
            $('.marketing-executive-job').toggle();
        });
    });
</script>
<script>
    $(document).ready(function(){
        $(window).scroll(function() { // check if scroll event happened
            if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
                $(".navbar").css("background-color", "#f8f8f8"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
            } else {
                $(".navbar").css("background-color", "transparent"); // if not, change it back to transparent
            }
        });
    });
</script>
</body>
</html>