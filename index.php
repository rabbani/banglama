<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
          integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text" rel="stylesheet">
    <script type="text/javascript" src="js/modernizr.custom.86080.js"></script>
    <title> Bangla Ma Design</title>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg" style="z-index: 2000">
    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    About
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="whoweare.php">Who we are</a>
                    <a class="dropdown-item" href="missionvision.php">Mission & Vision</a>
                    <a class="dropdown-item" href="profile.php">Company Profile</a>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Message
                    </a>
                    <div class="dropdown-menu message" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="chairman.php">Chairman Message</a>
                        <a class="dropdown-item" href="md.php">Managing Director message</a>

                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="services.php">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="project.php">Project</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="gallery.php"> Gallery </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="career.php">Career</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact.php"> Contact Us</a>
            </li>

        </ul>
    </div>
</nav>


<ul class="cb-slideshow">
    <li><span></span>
        <div><h3></h3></div>
    </li>
    <li><span></span>
        <div><h3> Interior Design</h3></div>
    </li>
    <li><span></span>
        <div><h3>Construction</h3></div>
    </li>
    <li><span></span>
        <div><h3>Event Management</h3></div>
    </li>
    <li><span></span>
        <div><h3>Event Management </h3></div>
    </li>
    <li><span></span>
        <div><h3>Stall Design & Decoration</h3></div>
    </li>
</ul>
<div class="greetings">
    <div class="greetings-text">
        Welcome To <br> <span class="banglama"><span style="color: #0f7e30;">Bangla</span><span
                    style="color: red;">Ma</span> Design Ltd. </span>
    </div>

</div>
<div class="down-arrow">
    <a href="#video-section" class="btn btn-danger"><i class="fas fa-arrow-down"></i></a>
</div>

<div class="container-fluid" id="video-section">
    <div class="row">
        <div class="body-video">
            <!--            <img src="images/3.jpg" alt="">-->
<!--            <video height="550" loop autoplay muted>-->
<!--                <source src="video/Vedio.mp4" type="video/mp4">-->
<!---->
<!--            </video>-->
            <iframe style="width: 100%; height: 30em" src="https://www.youtube.com/embed/qxLftn-zQhc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="footer row p-4">
        <div class="col text-center br-1">
            <div class="footer-logo">
                <img src="images/logo.png" alt="logo">
            </div>
            <br>
            <a href="http://banglamadesign.com:2095/" target="_blank" class="btn btn-outline-danger btn-sm btn-webmail">WebMail</a>
        </div>
        <div class="col text-center">
            <h3>
                Follow Us On
            </h3>
            <div class="social">
                <a href="https://www.facebook.com/banglamadesignltd/" target="_blank"><i class="fab fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UCQmFWII5iS1rF9VfNAkgapw" target="_blank"><i class="fab fa-youtube"></i></a>
                <i class="fab fa-linkedin"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-google-plus-square"></i>
                <i class="fab fa-instagram"></i>
            </div>
        </div>
        <div class="col">
            <h6 class="text-danger">
                Contact Info
            </h6>
            <p class="footer-address">
                <i class="fas fa-home text-danger"></i> 60 Purana Palton Line, Dhaka-1217
                <br><i class="fas fa-phone text-danger"></i> +88028312417, 9352282
                <br><i class="fas fa-mobile-alt text-danger"></i> &nbsp;+88 01936208414
                <br><i class="fas fa-envelope-open text-danger"></i> info@banglamadesign.com | banglamadesignltd@gmail.com
            </p>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
        integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
        crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
      integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

</body>
</html>