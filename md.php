<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
          integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Coda|Tangerine" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text" rel="stylesheet">
    <script type="text/javascript" src="js/modernizr.custom.86080.js"></script>
    <title> BanglaMa Design</title>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg" style="z-index: 2000">
    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    About
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="whoweare.php">Who we are</a>
                    <a class="dropdown-item" href="missionvision.php">Mission & Vision</a>
                    <a class="dropdown-item" href="profile.php">Company Profile</a>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Message
                    </a>
                    <div class="dropdown-menu message" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="chairman.php">Chairman Message</a>
                        <a class="dropdown-item" href="md.php">Managing Director message</a>

                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="services.php">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="project.php">Project</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="gallery.php"> Gallery </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="career.php">Career</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact.php"> Contact Us</a>
            </li>

        </ul>
    </div>
</nav>

<div class="chairman-body">
    <div class="body-shade">
        <div class="container">
            <div class="row my-5">
                <div class="col-md-8">
                    <h3 class="title-text text-center pb-2 md-title"> Managing Director Message </h3>
                    <p class="chairman-text text-justify">It is my pleasure to introduce some words about BanglaMa
                        Design Ltd.
                        BanglaMa Design Ltd. has emerged as a formidable force in Interior, Construction and Event
                        Management Section, led by talented and dedicated professionals.
                        This dedicated workforce has imparted their broad-based experience, and leadership skills in
                        shaping and building this Organization.
                        Through these efforts, we keep achieving our vision of promoting excellence in contraction and
                        fulfil our mission of serving the Interior, Construction and Event Management Section in
                        Bangladesh.
                        Extending my Best wishes
                    </p>
                    <p style="font-size: 1.6em; margin-bottom: 0"> Sanjoy Kumar Ghosh </p>
                    <p class="signature chairman-text">
                        Manging Director <br>
                        BanglaMa Design Ltd.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="images/MD%20Message.JPG" class="img-fluid rounded img-thumbnail" alt="">
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="our-clients">
        <div class="our-clients-title text-center">Our Clients</div>
        <div class="our-client-list text-center row">
            <div class="owl-carousel">
                <div class="col">
                    <img src="images/c1.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c2.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c3.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c4.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c5.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c6.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c7.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c8.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c9.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c10.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c11.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="footer row p-4">
        <div class="col text-center br-1">
            <div class="footer-logo">
                <img src="images/logo.png" alt="logo">
            </div>
            <br>
            <a href="http://banglamadesign.com:2095/" target="_blank" class="btn btn-outline-danger btn-sm btn-webmail">WebMail</a>
        </div>
        <div class="col text-center">
            <h3>
                Follow Us On
            </h3>
            <div class="social">
                <a href="https://www.facebook.com/banglamadesignltd/" target="_blank"><i class="fab fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UCQmFWII5iS1rF9VfNAkgapw" target="_blank"><i class="fab fa-youtube"></i></a>
                <i class="fab fa-linkedin"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-google-plus-square"></i>
                <i class="fab fa-instagram"></i>
            </div>
        </div>
        <div class="col">
            <h6 class="text-danger">
                Contact Info
            </h6>
            <p class="footer-address">
                <i class="fas fa-home text-danger"></i> 60 Purana Palton Line, Dhaka-1217
                <br><i class="fas fa-phone text-danger"></i> +88028312417, 9352282
                <br><i class="fas fa-mobile-alt text-danger"></i> &nbsp;+88 01936208414
                <br><i class="fas fa-envelope-open text-danger"></i> info@banglamadesign.com | banglamadesignltd@gmail.com
            </p>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
        integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
        crossorigin="anonymous"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>
<script>
    $(document).ready(function(){
        $(window).scroll(function() { // check if scroll event happened
            if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
                $(".navbar").css("background-color", "#f8f8f8"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
            } else {
                $(".navbar").css("background-color", "transparent"); // if not, change it back to transparent
            }
        });
    });
</script>
</body>
</html>