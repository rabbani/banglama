<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
          integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css"
          integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Coda|Tangerine" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Markazi+Text" rel="stylesheet">
    <script type="text/javascript" src="js/modernizr.custom.86080.js"></script>
    <title> BanglaMa Design</title>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg" style="z-index: 2000">
    <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    About
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="whoweare.php">Who we are</a>
                    <a class="dropdown-item" href="missionvision.php">Mission & Vision</a>
                    <a class="dropdown-item" href="profile.php">Company Profile</a>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Message
                    </a>
                    <div class="dropdown-menu message" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="chairman.php">Chairman Message</a>
                        <a class="dropdown-item" href="md.php">Managing Director message</a>

                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="services.php">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="project.php">Project</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="gallery.php"> Gallery </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="career.php">Career</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="contact.php"> Contact Us</a>
            </li>

        </ul>
    </div>
</nav>

<div class="services-body">
    <div class="body-shade">
        <div class="container">
            <div class="row my-5">
                <div class="col-md-4">
                    <div class="services-image">
                    <img src="images/architructural.jpeg" class="img-fluid" alt="image">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="services-title">
                        Architectural Design
                    </div>
                    <p class="services-text">Results-Oriented Approach. BanglaMa architects provide the hands on project
                        management that is critical to your project's success. Great project management involves keeping
                        the design team on task, details in order, and budgets on target and schedules on track.
                        Exceptional project management goes beyond organizational skills and includes the ability to
                        anticipate obstacles and respond proactively to avoid them. BanglaMa Design has a specialization
                        in their respective field.
                    </p>
                    <div class="architectural-more-text services-text">
                        <ul>
                            <li>Duplex Building</li>
                            <li>Triplex Building</li>
                            <li>High Rise Building</li>
                            <li>Semi Pukka House</li>
                            <li>Commercial Building</li>
                            <li>Mosque Design</li>
                            <li>Gate Design</li>
                            <li>Patrol Pump Design</li>
                        </ul>
                        <p>
                            BanglaMa Design Ltd. is a professionally managed firm and we have a team of highly qualified
                            managed architects and engineers, skilled technicians / tradesman with enough skilled /
                            semi-skilled personnel’s to execute the job. We have sufficient tools, plants, and
                            equipment’s to cater for the work. </p>
                    </div>
                    <button class="btn btn-danger btn-sm ml-auto more-btn btn-right" id="architectural-btn" type="button"> More <i
                                class="fas fa-arrow-alt-circle-down"></i></button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="services-title">
                        Structural Design
                    </div>
                    <p class="services-text">
                        BanglaMa Design Ltd. is established for build your dream. Our experience and capabilities in
                        providing structural design solutions are matched only by the high caliber of our engineering
                        professionals. Our successful project track record includes new and renovated commercial,
                        institutional, industrial and recreational buildings and facilities.
                    </p>
                    <div class="structural-more-text services-text">
                        <p>
                            We design specialized
                            structures to suit the complex project requirements of our municipal, electrical utility,
                            petrochemical, offshore, transportation and telecommunications clients. Our team is here to
                            listen to your needs. With our proven ability to meet unusual challenges, provide innovative
                            solutions, and exceed client expectations with the value of our services, we are able to
                            meet
                            all of your project goals.
                        </p>
                    </div>
                    <button class="btn btn-danger btn-sm more-btn" id="structural-btn" type="button"> More <i
                                class="fas fa-arrow-alt-circle-down"></i></button>

                </div>
                <div class="col-md-4">
                    <div class="services-image">
                    <img src="images/structural-design-engineer.jpg" class="img-fluid" alt="image">
                    </div>
                </div>
            </div>
            <div class="row my-5">
                <div class="col-md-4">
                    <div class="services-image">
                    <img src="images/Cons%20and%20Civil%20Works.png" class="img-fluid" alt="image">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="services-title">
                        Construction & Civil Works
                    </div>
                    <p class="services-text">Civil Works & Construction is committed and focuses on helping customers to
                        best services that are tailored for the needs. This means getting ever closer to customers,
                        understanding their lifestyles and with our customers support, we build long-term
                        partnerships.</p>
                    <div class="construction-more-text services-text">
                        <p>
                            Our mission is to provide HQUSACE staff support; and to direct the technical aspects of
                            engineering, construction management, environmental protection and restoration, operations,
                            maintenance and repair activities of USACE missions worldwide. Although we are in the Civil
                            Works Directorate, our mission areas support the entire Corps of Engineers: Civil Works
                            (CW), Military Programs (MP), and Work for Others (WFO), Support for Others (SFO),
                            Installation Support and Environmental Programs. We develop Technical Policy; develop and
                            integrate new technologies with our existing technology base; and manage all technical
                            aspects of our military and civil infrastructure and water resources missions. We are also
                            responsible for all policy for architecture, and are responsible for assuring all projects
                            are value engineered. Two special assistants provide day to day support to the USACE Civil
                            and Military Programs and support to other agencies. The USACE Engineering & Construction
                            Community is a key player and in many cases leading the way in new and exciting efforts
                            addressing Hurricane Protection, Military Transformation, Building Information Modeling,
                            flood risk management and numerous other efforts of the Corps of Engineers
                        </p>
                    </div>
                    <button class="btn btn-danger btn-sm ml-auto more-btn btn-right" id="construction-btn" type="button"> More <i
                                class="fas fa-arrow-alt-circle-down"></i></button>

                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="services-title">
                        Interior Design & Decoration:
                    </div>
                    <p class="services-text">
                        Interior Design & Decorations carries with it the power to form potent first impressions,
                        setting expectations for the experience to come for guests, customers and clients. Creating
                        exactly the ambience you desire is a responsibility and privilege BanglaMa Design Ltd. never
                        takes lightly.
                    </p>
                    <div class="interior-more-text services-text">
                        <p>
                            Lasting; Enduring; Striking the balance between inspired and sensible, our designs increase
                            your building's market value and enhance the user experience within it. Whether you have a
                            small public project or a world-class destination resort, we design to meet the test of
                            time.
                            Inventive; Resourceful; Attentive; What is it like to work with us? We are detail-oriented
                            and committed to quality design, while exploring and creating exceptional spaces for you.
                            Count on us to meet deadlines and stay on budget.
                            Sustainable; Responsible; We apply sustainable best practices to every project, from day
                            lighting and air quality to interior finish materials selection. Our designers guide clients
                            toward choices that are not only durable and beautiful, but responsibly consider the impact
                            to future generations.
                            Right-Sized Services; From small, professional offices and senior housing communities to
                            luxury hotels, we work within your scope and budget. As full-service interior designers, we
                            can assist with furniture, fixtures and equipment selection and specifications, as well as
                            guiding you through development of a Property Improvement Plan (PIP).
                            Architecture + Interior Design; Better Together; With our collaborative team of architects
                            and interior designers, total project design is elevated to a holistic creation that is
                            highly cost-effective and fully integrated to create precisely the lasting impression you
                            seek.
                            We promise an interior design solution that delivers the on-time and on-budget statement of
                            quality your project deserves. To learn more about how BanglaMa Design Ltd. interior
                            designers can help your project make the perfect first – and lasting – impression, please
                            contact us for a consultation. You will find our talent to be as solid as our commitment to
                            you.
                        </p>
                        <ul>
                            <li>Interiors Services</li>
                            <li>Interior Design</li>
                            <li>Furniture, Fixtures and Equipment Selection and Specification</li>
                            <li>Interior Decoration in Bangladesh (Office, Shop & Home)</li>
                            <li>Office Furniture & Home Furniture</li>
                            <li>Office Workstation Desk</li>
                            <li>Office Partition panels / Office partition System</li>
                            <li>Thai partition / Glass partition Wall, Door</li>
                            <li>Chair & Sofa</li>
                            <li>Kitchen Cabinet & Wall Cabinet</li>
                            <li>False Ceiling & Design Drop Ceiling</li>
                            <li>Vertical blind, Venetian blind & Roller Blind,</li>
                            <li>Painting work & Wall paper,</li>
                            <li>Wooden Floor / PVC Floor / Carpet</li>
                        </ul>
                    </div>
                    <button class="btn btn-danger btn-sm more-btn" id="interior-btn" type="button"> More <i
                                class="fas fa-arrow-alt-circle-down"></i></button>

                </div>
                <div class="col-md-4">
                    <div class="services-image">
                    <img src="images/Interior%20Design.jpg" class="img-fluid" alt="image">
                    </div>
                </div>
            </div>

            <div class="row my-5">
                <div class="col-md-4">
                    <div class="services-image">
                    <img src="images/3D%20V.jpg" class="img-fluid" alt="image">
                    </div>

                </div>
                <div class="col-md-8">
                    <div class="services-title">
                        3D Visualizations:
                    </div>
                    <p class="services-text">With realistic 3D visualizations you can get a comprehensive simulation of
                        the building or product in a manner as it would appear in real-time. This is conducted during
                        the design phase, which makes it easier for you to have a better understanding of every floor,
                        critical areas, landscape, textures, and elevations. </p>
                    <div class="visualizations-more-text services-text">
                        <p>Moreover, you can identify the flaws and issues in designing and rectify the same immediately
                            before it is being implemented. Design Associates has skilled artists. They will give you
                            the opportunity to visualize your designs.</p>
                    </div>
                    <button class="btn btn-danger btn-sm ml-auto more-btn btn-right" id="visualizations-btn" type="button"> More
                        <i
                                class="fas fa-arrow-alt-circle-down"></i></button>

                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <div class="services-title">
                        Graphic Design:
                    </div>
                    <p class="services-text">Graphic design is the process of visual communication and problem-solving
                        using one or more of typography, photography and illustration. The field is considered a subset
                        of visual communication and communication design, but sometimes the term "graphic design" is
                        used synonymously. Graphic designers create and combine symbols, images and text to form visual
                        representations of ideas and messages.</p>
                    <div class="graphics-more-text services-text">
                        <p>They use typography, visual arts and page layout techniques to create visual compositions.
                            Common uses of graphic design include corporate design (logos and branding), editorial
                            design (magazines, newspapers and books), way finding or environmental design, advertising,
                            web design, communication design, product packaging and signage.</p>
                    </div>
                    <button class="btn btn-danger btn-sm more-btn" id="graphics-btn" type="button"> More <i
                                class="fas fa-arrow-alt-circle-down"></i></button>

                </div>
                <div class="col-md-4">
                    <div class="services-image">
                    <img src="images/graphicdesign.png" class="img-fluid" alt="image">
                    </div>

                </div>
            </div>

            <div class="row my-5">
                <div class="col-md-4">
                    <div class="services-image">
                    <img src="images/Event.jpg" class="img-fluid" alt="image">
                    </div>

                </div>
                <div class="col-md-8">
                    <div class="services-title">
                        Event Management:
                    </div>
                    <p class="event-text services-text">BanglaMa Design Ltd. is the premier event management companies
                        in Bangladesh and is regarded as one of the best by all of our clients and we ensure to retain
                        this acknowledgement in our industry by continuing in delivering the events that you dream
                        of.</p>
                    <div class="event-more-text services-text">
                        <p>We combine international expertise and ideas with local knowledge to create a distinctive and
                            memorable event experience for you.
                            We offers complete event management services from A- Z or provide you with the right
                            equipment rentals for a perfect event.
                            We pride ourselves on our attention to every detail with no request being too large or too
                            small for any of our clients.</p>

                        <p>Services:</p>
                        <ul>
                            <li>Seminar</li>
                            <li>Exhibition</li>
                            <li>Cultural Program,</li>
                            <li>Sports Program,</li>
                            <li>Marriage Ceremony,</li>
                            <li>Products Lunching</li>
                            <li>Stall Design & Decoration</li>
                            <li>& All Kind of Event Management.</li>

                        </ul>
                    </div>
                    <button class="btn btn-danger btn-sm ml-auto more-btn btn-right" id="event-btn" type="button"> More
                        <i
                                class="fas fa-arrow-alt-circle-down"></i></button>

                </div>
            </div>

<!--            <div class="row">-->
<!--                <div class="col-md-8 row">-->
<!--                    <div class="services-title">-->
<!--                        Consultancy Services:-->
<!--                    </div>-->
<!--                    <p class="services-text">BanglaMa Design Ltd. provides professional consultancy & project management-->
<!--                        service. We deliver value for client’s money by providing the highest standards of project-->
<!--                        management, utilizing our most experienced project managers to realize your business benefits-->
<!--                        and deliver projects in time, cost and quality.-->
<!--                        </p>-->
<!--                    <div class="consultancy-more-text services-text">-->
<!--                        <p>BanglaMa Design Ltd. provides the following services as a consulting firm.-->
<!--                        <ul>-->
<!--                            <li>Soil Test</li>-->
<!--                            <li>Piling</li>-->
<!--                            <li>Estimation</li>-->
<!--                            <li>Quality Control</li>-->
<!--                            <li>Site Supervision</li>-->
<!--                            <li>Electrical Drawing</li>-->
<!--                            <li>Plumbing Drawing</li>-->
<!--                            <li>RAJUK Sheet</li>-->
<!--                            <li>RAJUK Approval</li>-->
<!--                            <li>Interior & Exterior Design</li>-->
<!--                            <li>BOQ (Bill of Quantity)</li>-->
<!--                        </ul>-->
<!--                        <p>-->
<!--                    </div>-->
<!--                    <button class="btn btn-danger btn-sm more-btn" id="consultancy-services-btn" type="button"> More <i-->
<!--                                class="fas fa-arrow-alt-circle-down"></i></button>-->
<!--                </div>-->
<!---->
<!--                <div class="col-md-4">-->
<!--                    <img src="images/consultancy-services.jpg" class="img-fluid img-thumbnail" alt="image">-->
<!--                </div>-->
<!--            </div>-->





        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="our-clients">
        <div class="our-clients-title text-center">Our Clients</div>
        <div class="our-client-list text-center row">
            <div class="owl-carousel">
                <div class="col">
                    <img src="images/c1.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c2.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c3.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c4.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c5.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c6.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c7.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c8.png" alt="">
                </div>
                <div class="col">
                    <img src="images/c9.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c10.jpg" alt="">
                </div>
                <div class="col">
                    <img src="images/c11.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="footer row p-4">
        <div class="col text-center br-1">
            <div class="footer-logo">
                <img src="images/logo.png" alt="logo">
            </div>
            <br>
            <a href="http://banglamadesign.com:2095/" target="_blank" class="btn btn-outline-danger btn-sm btn-webmail">WebMail</a>
        </div>
        <div class="col text-center">
            <h3>
                Follow Us On
            </h3>
            <div class="social">
                <a href="https://www.facebook.com/banglamadesignltd/" target="_blank"><i class="fab fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UCQmFWII5iS1rF9VfNAkgapw" target="_blank"><i class="fab fa-youtube"></i></a>
                <i class="fab fa-linkedin"></i>
                <i class="fab fa-twitter"></i>
                <i class="fab fa-google-plus-square"></i>
                <i class="fab fa-instagram"></i>
            </div>
        </div>
        <div class="col">
            <h6 class="text-danger">
                Contact Info
            </h6>
            <p class="footer-address">
                <i class="fas fa-home text-danger"></i> 60 Purana Palton Line, Dhaka-1217
                <br><i class="fas fa-phone text-danger"></i> +88028312417, 9352282
                <br><i class="fas fa-mobile-alt text-danger"></i> &nbsp;+88 01936208414
                <br><i class="fas fa-envelope-open text-danger"></i> info@banglamadesign.com | banglamadesignltd@gmail.com
            </p>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
        integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
        crossorigin="anonymous"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/custom.js"></script>


<script>
    $('document').ready(function () {
        $('.architectural-more-text').hide();
        $('.structural-more-text').hide();
        $('.construction-more-text').hide();
        $('.interior-more-text').hide();
        $('.visualizations-more-text').hide();
        $('.graphics-more-text').hide();
        $('.event-more-text').hide();
        $('.consultancy-more-text').hide();

        $('#architectural-btn').on('click', function () {
            $('.architectural-more-text').toggle();
        });

        $('#structural-btn').on('click', function () {
            $('.structural-more-text').toggle();
        });

        $('#construction-btn').on('click', function () {
            $('.construction-more-text').toggle();
        });

        $('#interior-btn').on('click', function () {
            $('.interior-more-text').toggle();
        });

        $('#visualizations-btn').on('click', function () {
            $('.visualizations-more-text').toggle();
        });
        $('#graphics-btn').on('click', function () {
            $('.graphics-more-text').toggle();
        });
        $('#event-btn').on('click', function () {
            $('.event-more-text').toggle();
        });
        $('#consultancy-services-btn').on('click', function () {
            $('.consultancy-more-text').toggle();
        });
    })
</script>
<script>
    $(document).ready(function(){
        $(window).scroll(function() { // check if scroll event happened
            if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
                $(".navbar").css("background-color", "#f8f8f8"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
            } else {
                $(".navbar").css("background-color", "transparent"); // if not, change it back to transparent
            }
        });
    });
</script>
</body>
</html>